document.getElementById("btnRandom").addEventListener("click", function() {
    let cantidadNumeros = parseInt(document.getElementById("cantNum").value);
    let arregloAleatorio = llenarArregloAleatorio(cantidadNumeros);
    mostrarArray(arregloAleatorio);
    mostrarValoresPares(arregloAleatorio);
    let porcentajePares = calcularPorcentajePares(arregloAleatorio);
    let porcentajeImpares = 100 - porcentajePares;
    let esSimetrico = verificarSimetriaNumerosAleatorios(arregloAleatorio);

    let resultado = `Porcentaje de pares: ${porcentajePares.toFixed(2)}% \nPorcentaje de impares: ${porcentajeImpares.toFixed(2)}%\n `;
    resultado += `¿Es simétrico?: ${esSimetrico ? 'Sí' : 'No'}`;
    
    
    document.getElementById("results").innerText = resultado;
});

function llenarArregloAleatorio(longitud) {
    let arregloAleatorio = [];
    for (let con = 0; con < longitud; ++con) {
        arregloAleatorio.push(Math.floor(Math.random() * 100)); // genera números aleatorios entre 0 y 100
    }
    return arregloAleatorio;
}

function mostrarArray(arreglo) {
    let cmbNum = document.getElementById("cmbNum");
    cmbNum.innerHTML = ""; // Limpiar opciones previas
    for (let i = 0; i < arreglo.length; ++i) {
        let option = document.createElement("option");
        option.value = arreglo[i];
        option.text = arreglo[i];
        cmbNum.appendChild(option);
    }
}

function mostrarValoresPares(arreglo) {
    for (let con = 0; con < arreglo.length; ++con) {
        if (arreglo[con] % 2 === 0) {
            console.log(arreglo[con]);
        }
    }
}

function calcularPorcentajePares(arreglo) {
    let contadorPares = 0;
    for (let i = 0; i < arreglo.length; ++i) {
        if (arreglo[i] % 2 === 0) {
            contadorPares++;
        }
    }
    return (contadorPares / arreglo.length) * 100;
}

function verificarSimetriaNumerosAleatorios(arreglo) {
    let contadorPares = 0;
    let contadorImpares = 0;
    
    for (let i = 0; i < arreglo.length; i++) {
        if (arreglo[i] % 2 === 0) {
            contadorPares++;
        } else {
            contadorImpares++;
        }
    }
    
    let porcentajePares = (contadorPares / arreglo.length) * 100;
    let porcentajeImpares = (contadorImpares / arreglo.length) * 100;

    return Math.abs(porcentajePares - porcentajeImpares) <= 20;
}
